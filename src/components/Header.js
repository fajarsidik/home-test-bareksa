import '../assets/scss/Header.scss'
import logo from '../assets/img/bareska-logo@2x.png'
import notification from '../assets/img/ic-notification.svg'
import settings from '../assets/img/ic-settings.svg'
import search from '../assets/img/ic-search.svg'
import arrow from '../assets/img/ic-arrow-down.svg'

function Header(props){
    return(
        <header>

           <div className="flex items-center item">
                <div className="logo">
                    <a href="/#" value="logo-link">
                        <img src={logo} alt="Logo Bareska" />
                    </a>
                </div>

                <div className="profile">
                    <div className="avatar">
                        <span>RH</span>
                    </div>
                    <div className="user">
                        <p className="name">Reinhart H</p>
                        <p className="address">Kemang, Jakarta</p>
                    </div>
                    <img src={arrow} alt="" />
                </div>
            </div>
           
           <div className="flex items-center item">
                <div className="search-form mr-6">
                    <div className="form-group">
                        <input type="text" className="search form-input md" placeholder='Search text' />
                        <button>
                            <img src={search} alt="" />
                        </button>
                    </div>
                </div>

                <div className="control">
                    <a href="/#" className="notification has-notification mr-4">
                        <img src={notification} alt="" />
                    </a>
                    <a href="/#">
                        <img src={settings} alt="" />
                    </a>
                </div>
            </div>


        </header>   
    )
}

export default Header