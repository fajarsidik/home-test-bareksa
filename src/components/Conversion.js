import '../assets/scss/Body.scss'
import { Chart as ChartJS, ArcElement, Tooltip, Legend } from 'chart.js';
import { Pie } from 'react-chartjs-2';
ChartJS.register(ArcElement, Tooltip, Legend);
export const data = {
    labels: ['Item#1', 'Item#2', 'Item#3', 'Item#4'],
    datasets: [
      {
        label: '# of Votes',
        data: [2000, 2500, 300, 1900],
        backgroundColor: [
          '#725E9C',
          '#5C8F94',
          '#EBA45E',
          '#E4EAEB'
        ],
        borderWidth: 0,
      },
    ],
  };

export const option = {
    plugins: {
        title: {
          display: false,          
        },
       legend: { 
          display: true, 
          position: "bottom",
        }
    }   
}
function Conversion(props){
    return(
        <Pie data={data} options = {option}  />
    )
}

export default Conversion