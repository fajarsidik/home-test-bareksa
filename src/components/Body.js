import '../assets/scss/Body.scss'
import Conversion from './Conversion'
import Users from './Users'
import Revenue from './Revenue'
import Calendar from './Calendar'
import Orders from './Orders'
import icOption from '../assets/img/ic-option-chart.svg'
import icArrowUp from '../assets/img/ic-arrow-up.svg'
import icCalendar from '../assets/img/ic-calendar.svg'


function Body(props){

    return(
        <section className="body">
            <div className="date-info">
                <p>8 April 2021</p>
            </div>

            <div className="flex container">
                <div className="panel one-quarter">
                    <div className="title">
                        <h3>Conversion</h3>
                        <a href="/#">
                            <img src={icOption} alt="" />
                        </a>
                    </div>
                    <div className="conversion">
                        <Conversion />
                    </div>
                </div>

                <div className="panel one-quarter">
                    <div className="title">
                        <h3>Users</h3>
                        <a href="/#">
                            <img src={icOption} alt="" />
                        </a>
                    </div>
                    <div className="conversion">
                        <Users />
                    </div>
                </div>

                <div className="panel is-half">
                    <div className="title">
                        <h3>Revenue</h3>
                        <div className="form-group">
                            <input type="text" value="Feb - Mar 2021" className="text-black font-semibold sm" />
                            <button>
                                <img src={icCalendar} alt="" />
                            </button>
                            
                        </div>
                    </div>
                    <div className="conversion">
                        <Revenue />
                    </div>

                    <div className="footer">
                        <span>Total Revenue</span>
                        <p className="total">$76685.41</p>
                        <p className="percentage">
                            <img src={icArrowUp} alt="" className='mr-1' />
                            7,00%
                        </p>
                    </div>
                </div>
            </div>

            <div className="flex container">
                <div className="panel one-quarter">
                    <Calendar />
                    <div className="flex justify-center">
                        <button className="btn btn-cancel">Cancel</button>
                        <button className="btn btn-filter">Filter</button>
                    </div>
                </div>

                <div className="panel three-quarter">
                    <div className="title">
                        <h3>Orders</h3>                        
                    </div>
                    <Orders />
                </div>
            </div>


        </section>
    )
}

export default Body