import DataTable from "react-data-table-component";
import styled from "styled-components";
import useFetch from "react-fetch-hook"
import Moment from 'react-moment';


const columns = [
  {
    id: 1,
    name: "Order Number",
    selector: (row) => row.order_id,
    sortable: true,
    reorder: true
  },
  {
    id: 2,
    name: "Status",
    selector: (row) => row.status,
    sortable: true,
    reorder: true,
    cell: (row) => (
      <StyledCell className={getCssClass(row.status)}>
        {row.status}
      </StyledCell>
    )
  },
  {
    id: 3,
    name: "Operator",
    selector: (row) => row.full_name,
    sortable: true,    
    reorder: true,
    
  },
  {
    id: 4,
    name: "Location",
    selector: (row) => row.location,
    sortable: true,    
    reorder: true,
    
  },
  {
    id: 5,
    name: "Start Date",
    selector: (row) => row.start_date,
    sortable: true,    
    reorder: true,
    cell: (row) => (
      formatDate(row.start_date)
    )
    
  },
  {
    id: 5,
    name: "Due Date",
    selector: (row) => row.due_date,
    sortable: true,    
    reorder: true,
    cell: (row) => (
      formatDate(row.due_date)
    )
  }
];
const StyledCell = styled.div`
  &.pending {
    background: #E59849;   
  }
  &.completed {
    background: #789764;
  }
  &.canceled {
    background: #D66D4B;
  }
 
  &.pending,
  &.completed,
  &.canceled{
    padding: 8px 16px;
    border-radius: 4px;
    color: #fff;
    font-family: 'Montserrat', sans-serif !important;
  }
`;

function getCssClass(value) {  
  return value;
}

function formatDate(value){
  return <Moment format="DD/MM/YY hh:mm">
          {value}
        </Moment>
}

function Orders(props){
  
  const {data} = useFetch("https://ae1cdb19-2532-46fa-9b8f-cce01702bb1e.mock.pstmn.io/takehometest/web/dashboard");  
  
  if(data){
    return <DataTable
        title=""
        columns={columns}
        data={data.data.orders}
        defaultSortFieldId={1}          
        pagination
        selectableRows
        paginationPerPage={5}  
        responsive={true}
      />
  }
  
}

export default Orders
